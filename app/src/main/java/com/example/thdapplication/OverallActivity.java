package com.example.thdworkapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.thdworkapplication.DataModels.Job;

import java.util.ArrayList;
import java.util.List;

/**
 * OVERALL ACTIVITY
 *
 * DataManagement
 */
public class OverallActivity extends AppCompatActivity {

    private TextView resultView;
    protected DatabaseManager DATABASE;
    protected List<Job> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overall);
        this.resultView = (TextView) findViewById(R.id.resultView);
        updateDisplay();
        DATABASE = DatabaseManager.getInstance(getApplicationContext());
        data = new ArrayList<Job>();
    }

    @Override
    public void onBackPressed(){
        //Clearing all the variables to free up memory
        DATABASE = null;
        resultView.setText("");
    }

    /**
     * This method will extract data according to specific criteria that is provided.
     * The data will be pre-sorted to provide a specific result that is based on a pre-set template.
     *
     */
    private void getDataFromDatabase() {
        if (checkDatabase()) {

        }
    }

    /**
     * Checking to insure that the database passed is populated and will not crash when extracting data.
     *
     * @return
     */
    protected boolean checkDatabase() {
        if (DATABASE == null) {
            return true;
        } else {
            return false;
        }
    }
    private static Supervisor supervisor;
    public void todayButton(View view) {
        //Report for the Day
        String date = supervisor.calculateYearToDate(); // Will return the current system time.

        //Check array
        if (!data.isEmpty()){
            data = DATABASE.getEverythingFromDatabase();
        }  

    }


    public void weekButton(View view) {
        //Weekly Report
    }


    public void monthButton(View view) {
       //Report for the month
    }


    /**
     * Update display will be called multiple times though the life the application.
     * The textview updates with the productivity percentage a the current period that you have selected.
     */
    private void updateDisplay() {
        String temp = " Display has been updated";
        resultView.setText(temp);
    }
}
