package com.example.thdworkapplication.DataModels;

import java.util.Date;

public class Job {
    private String date;
    private int repairLite;
    private int repair;
    private int ber;
    private int customer;
    private int store;
    private int eval;


    public Job(){

    }

    public Job(String date, int repairLite, int repair, int ber, int customer, int store, int eval) {
        this.date = date;
        this.repairLite = repairLite;
        this.repair = repair;
        this.ber = ber;
        this.customer = customer;
        this.store = store;
        this.eval = eval;
    }

    public String getDate() {
        return date;
    }

    public int getEval() {
        return eval;
    }

    public void setEval(int eval) {
        this.eval = eval;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getRepairLite() {
        return repairLite;
    }

    public void setRepairLite(int repairLite) {
        this.repairLite = repairLite;
    }

    public int getRepair() {
        return repair;
    }

    public void setRepair(int repair) {
        this.repair = repair;
    }

    public int getBer() {
        return ber;
    }

    public void setBer(int ber) {
        this.ber = ber;
    }

    public int getCustomer() {
        return customer;
    }

    public void setCustomer(int customer) {
        this.customer = customer;
    }

    public int getStore() {
        return store;
    }

    public void setStore(int store) {
        this.store = store;
    }

    @Override
    public String toString() {
        return "Job{" +
                "date=" + date +
                ", repairLite=" + repairLite +
                ", repair=" + repair +
                ", ber=" + ber +
                ", customer=" + customer +
                ", store=" + store +
                '}';
    }
}
