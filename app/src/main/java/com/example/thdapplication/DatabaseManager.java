package com.example.thdapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.widget.Toast;

import com.example.thdworkapplication.DataModels.Job;

import java.util.ArrayList;
import java.util.List;

/**
 * Creating a single table to hold the data for a long period of time
 */
public class DatabaseManager extends SQLiteOpenHelper {
    private static final String LOG_TAG = "Database Manager ERROR ====>";
    private static final String TABLE_NAME = "DAILY_REPORT";
    private static final String CREATE_JOB_TIME_TABLE = "CREATE TABLE DAILY_REPORT (\n" +
            "  DATE_TIME INT NOT NULL," +
            "  REPAIR_LITE INT NOT NULL," +
            "  REPAIR INT NOT NULL," +
            "  BER INT NOT NULL," +
            "  CUSTOMER INT NOT NULL," +
            "  STORE INT NOT NULL" +
            ");";
    private static final String GET_EVERYTHING = "SELECT * FROM DAILY_REPORT;";
    private Context context;
    private static final String DATABASE_NAME = "WORK_HISTORY";
    private static final int DATABASE_VERISON = 1;


    /**
     * Singleton pattern.
     * Single instance of the database application is used to share the same
     * database between all intents.
     *
     * @param context
     * @return
     */
    public static synchronized DatabaseManager getInstance(Context context) {
        if (singleton == null) {
            singleton = new DatabaseManager(context.getApplicationContext());
        }
        return singleton;
    }

    /**
     * Private Constructor.
     * This part constructor is important for constructing the var that will be passed around
     * between views.
     *
     * @param context
     */
    private DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERISON);
        this.context = context;
    }

    // Called when the database connection is being configured.
    // Configure database settings for things like foreign key support, write-ahead logging, etc.
    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            db.setForeignKeyConstraintsEnabled(true);
        }
    }

    private static DatabaseManager singleton;

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(this.CREATE_JOB_TIME_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);
            onCreate(db);
        }
    }

    /**
     * Creating a new record in the database for jobs.
     * Each job will represent a single day of work preformed.
     *
     * @param job
     */
    public void createNewRecord(Job job) {
        SQLiteDatabase database = getWritableDatabase();

        database.beginTransaction();

        try {
            ContentValues values = new ContentValues();
            values.put("DATE_TIME", job.getDate().toString());
            values.put("REPAIR_LITE", job.getRepairLite());
            values.put("REPAIR", job.getRepair());
            values.put("BER", job.getBer());
            values.put("CUSTOMER", job.getCustomer());
            values.put("STORE", job.getStore());

            database.insert(TABLE_NAME, null, values);
            database.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
    }



    /**
     * Every record in the database is stored in the record format. Each day will be on job that was performed, to retrieve the information
     * the data will have to be process and extracted to breakdown data.
     * @return List<Job>
     */
    public List<Job> getEverythingFromDatabase() {
        List<Job> jobList = new ArrayList<Job>();
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(GET_EVERYTHING, null);
        database.beginTransaction();

        try {
            if (cursor.moveToFirst()) {
                do {
                    Job temp = new Job();
                    temp.setDate(cursor.getString(cursor.getColumnIndex("DATE_TIME")).toString());
                    temp.setRepairLite(cursor.getInt(cursor.getColumnIndex("REPAIR_LITE")));
                    temp.setRepair(cursor.getInt(cursor.getColumnIndexOrThrow("REPAIR")));
                    temp.setBer(cursor.getInt(cursor.getColumnIndexOrThrow("BER")));
                    temp.setCustomer(cursor.getInt(cursor.getColumnIndexOrThrow("CUSTOMER")));
                    temp.setStore(cursor.getInt(cursor.getColumnIndexOrThrow("STORE")));
                    jobList.add(temp);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {

        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return jobList;
    }

    public int howManyRecords(){
        int count = getEverythingFromDatabase().size();
        return count;
    }

    public void deleteRecord(Job job) {
        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();

        //String where clause to remove the correct record.
        String delete = "DELETE FROM DAILY_REPORT WHERE DATE_TIME = '" + job.getDate() + "';";
        try {
            //Find the record in the database
            database.delete("DAILY_REPORT", delete, null);
            //Removed the record from the database.
            database.setTransactionSuccessful();
            toastMessage("Record added");
        } catch (Exception e) {
            toastMessage("Failure check error.");
            e.printStackTrace();
        } finally {
            //Closing the database connection after the transaction is successful.
            database.endTransaction();
        }
    }

    public void toastMessage(String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);

        toast.show();
    }


}
