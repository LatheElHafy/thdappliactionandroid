package com.example.thdworkapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.thdworkapplication.DataModels.Job;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PreviousActivity extends AppCompatActivity {
//Testing
    private DatabaseManager databaseManager;
    private ListView listView ;
    private ArrayAdapter adapter;
    HashMap<String, List<Job>> datejob;
    private List<Job> jobs;
    private List<String> dates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous);
        databaseManager = DatabaseManager.getInstance(getApplicationContext());
        this.jobs = new ArrayList<Job>();
        this.dates = new ArrayList<String>();
        datejob = new HashMap<String,List<Job>>();
//        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.activity_previous,StringArray);
        listView = findViewById(R.id.listView);
        populateList();
        String [] users = {"something","something 1","something 2"};
        adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,dates);
        listView.setAdapter(adapter);

    }

    /**
     * Extract everything from the database and feed it into the list view and update accordingly.
     * The view will show the list of dates there were saved for each.
     * Everything that is sorted in the list will be displayed by the date they were created.
     */
    public void populateList(){
        //Extract everything from the database to-date.\
        jobs = databaseManager.getEverythingFromDatabase();
        populateDateList();
    }

    /**
     * This method will populate the list with the dates of all the entries.
     * The entries are sorted according to date, the newest to the oldest.
     *
     */
    public void populateDateList(){
        for (Job temp : jobs) {
            this.dates.add(temp.getDate());
        }
    }


    private String [] covertFromListToString(List<String> data){
//        String [] dataSet = new String{};

        return null;
    }

}
